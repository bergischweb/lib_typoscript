<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

/**
 * Lib TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY, 
    'Configuration/TypoScript', 
    'Lib Typoscript'
);

/**
 * add these fields to the rootline in order to make the meta tags work
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] = 'author,keywords,description'. ($GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields']?','.$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields']:'');

