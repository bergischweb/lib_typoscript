<?php

/***************************************************************
 * 
 * Extension Manager/Repository config file for ext "lib_typoscript".
 *
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'TypoScript Lib',
	'description' => 'A practical library of TypoScript scriptlets to be included in TypoScript templates',
	'category' => 'be',
	'author' => 'Stefan Padberg',
	'author_email' => 'post@bergische-webschmiede',
	'author_company' => 'Bergische Webschmiede',
	'state' => 'stable',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearcacheonload' => true,
	'version' => '1.0.1',
	'constraints' => array (
		'depends' => array (
			'typo3' => '6.2.0-9.9.99',
		),
		'conflicts' => array (
		),
		'suggests' => array (
		),
	),
);
